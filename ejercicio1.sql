﻿USE ciclistas;

-- 1 listar las edades de los ciclistas(sin repetidos) --
SELECT DISTINCT edad FROM ciclista;

-- 2 listar las edades de los ciclistas del equipo Artiach --
SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' ;

-- 3 listar las edades del equipo artich Y Amore Vita --
SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo= 'Amore Vita' ;

-- 4 listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30 --
SELECT DISTINCT dorsal FROM ciclista WHERE edad<25 OR edad>30;
-- con operador UNION
SELECT DISTINCT c.dorsal FROM ciclista c WHERE c.edad <25 UNION SELECT c.dorsal FROM ciclista c WHERE c.edad>30;
-- con operador not between
SELECT DISTINCT c.dorsal FROM ciclista c WHERE c.edad NOT BETWEEN 15 AND 30;

-- 5 listar los dorsales de los ciclistas cuya edad este entre 28 y 32 --
 SELECT c.dorsal FROM ciclista c WHERE c.edad BETWEEN 28 AND 32;

-- 6 indicame el nombre de los ciclistas que el numero de caracteres del nombre sea mayor que 8 --
 SELECT DISTINCT c.nombre FROM ciclista c WHERE CHAR_LENGTH (c.nombre)<10;

-- 7 Listar el nombre y el dorsal de todos los ciclistas mostrando un campo
-- nuevo denominado nombre mayusculas que debe mostrar el nombre en mayúsculas 
SELECT c.nombre, c.dorsal, UPPER( c.nombre) Nombre_en_mayusculas FROM ciclista c; 

-- 8 Listar todos los ciclistas que han llevado el maillot MGE (amarillo) en alguna etapa
SELECT DISTINCT * FROM lleva l WHERE l.código='MGE'; 

-- 9  indicame el numero de etapas que hay --
  SELECT DISTINCT l.dorsal FROM lleva l WHERE l.código='MGE';  

-- 10 listar el nombre de los puertos cuya altura sea mayor que 1500
    SELECT p.nompuerto FROM puerto p WHERE p.altura>'1500';

-- 11 listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y3000
      SELECT p.dorsal FROM puerto p WHERE p.pendiente>'8' OR p.altura BETWEEN 1800 AND 3000;
